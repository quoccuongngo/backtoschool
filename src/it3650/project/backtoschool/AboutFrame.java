package it3650.project.backtoschool;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AboutFrame extends JFrame {
	private int u = MyApp.ScreenUnit;
	private JPanel panel;
	private JLabel img1;
	private JLabel img2;
	private JLabel icon;
	private JEditorPane ep1;
	private JEditorPane ep2;
	private JButton btnBack;
	
	public AboutFrame() {
		initComponents();
	}
	
	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/bkg_about.jpg");
			
			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(MyApp.ScreenSize);
		panel.setLayout(null);
		
		btnBack = new JButton();
		btnBack.setBounds(u, u, 2*u, 2*u);
		ImageIcon ico = new ImageIcon("res/drawable/icon/back.png");
		Image img = ico.getImage().getScaledInstance(2*u, 2*u,
				Image.SCALE_SMOOTH);
		btnBack.setIcon(new ImageIcon(img));
		btnBack.setOpaque(false);
		btnBack.setContentAreaFilled(false);
		btnBack.setBorderPainted(false);
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MyApp.frmNavigation.setVisible(true);
				AboutFrame.this.dispose();
			}
		});
		panel.add(btnBack);
		
		icon = new JLabel();
		icon.setBounds(MyApp.ScreenSize.width - 10*u, u, 9*u, 5*u);
		ico = new ImageIcon("res/drawable/icon/icon_large.png");
		img = ico.getImage().getScaledInstance(9*u, 5*u,
				Image.SCALE_SMOOTH);
		icon.setIcon(new ImageIcon(img));
		panel.add(icon);
		
		img1 = new JLabel();
		img1.setBounds(10*u, 2*u, 5*u, 5*u);
		ico = new ImageIcon("res/drawable/background/quoccuong.jpg");
		img = ico.getImage().getScaledInstance(5*u, 5*u,
				Image.SCALE_SMOOTH);
		img1.setIcon(new ImageIcon(img));
		panel.add(img1);
		
		ep1 = new JEditorPane();
		ep1.setBounds(15*u, 2*u, 10*u, 4*u);
		ep1.setMargin(new java.awt.Insets(0, 15, 0, 0));
		ep1.setEditable(false);
		ep1.setOpaque(false);
		ep1.setContentType("text/html");
		ep1.setText("<html><body>"
				+ "<div style=\"text-align: left; font-size: 25px; color: white; font-family: Times New Roman\">"
					+ "Ngô Quốc Cường<br/>"
				+ "<div>"
				+ "<div style=\"text-align: left; font-size: 15px; color: white; font-family: Times New Roman\">"
					+ "20101212<br/>Lớp KSCLC-THCN-K55<br/>quoccuongbka@gmail.com"
				+ "<div>"
				+ "</body></html>");
		panel.add(ep1);
		
		img2 = new JLabel();
		img2.setBounds(14*u, 8*u, 5*u, 5*u);
		ico = new ImageIcon("res/drawable/background/vancong.jpg");
		img = ico.getImage().getScaledInstance(5*u, 5*u,
				Image.SCALE_SMOOTH);
		img2.setIcon(new ImageIcon(img));
		panel.add(img2);
		
		ep2 = new JEditorPane();
		ep2.setBounds(19*u, 8*u, 10*u, 4*u);
		ep2.setMargin(new java.awt.Insets(0, 15, 0, 0));
		ep2.setEditable(false);
		ep2.setOpaque(false);
		ep2.setContentType("text/html");
		ep2.setText("<html><body>"
				+ "<div style=\"text-align: left; font-size: 25px; color: white; font-family: Times New Roman\">"
					+ "Nguyễn Văn Công<br/>"
				+ "<div>"
				+ "<div style=\"text-align: left; font-size: 15px; color: white; font-family: Times New Roman\">"
					+ "20101187<br/>ĐTVT 05 - K55"
			+ "<div>"
				+ "</body></html>");
		panel.add(ep2);
		
		this.setIconImage(MyApp.icon);
		this.setUndecorated(true);
		this.setSize(MyApp.ScreenSize);
		this.add(panel);

		pack();
	}
}
