package it3650.project.backtoschool;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.sun.image.codec.jpeg.ImageFormatException;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

@SuppressWarnings("serial")
public class GameOverFrame extends JFrame {
	private int u = MyApp.ScreenUnit;
	private int level;
	private JPanel panel;
	private JPanel pn;
	private JLabel avt;
	private JLabel lblName;
	private JLabel lblSex;
	private JEditorPane epLevel;
	private JLabel lblDifficulty;
	private JLabel lblDate;
	private JButton btnReplay;
	private JButton btnShare;
	private JButton btnQuit;

	public GameOverFrame(int level) {
		this.level = level;
		initComponents();
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon(
					"res/drawable/background/bkg_game_over.jpg");

			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(MyApp.ScreenSize);
		panel.setLayout(null);

		pn = new JPanel() {
			ImageIcon icon = new ImageIcon(
					"res/drawable/background/bkg_certificate.jpg");

			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		pn.setBounds(7 * u, (int)(3.5*u), MyApp.ScreenSize.width - 14 * u, 10 * u);
		pn.setLayout(null);
		panel.add(pn);

		avt = new JLabel();
		avt.setBounds((int)(1.5*u), 5*u/2, 5*u, 5*u);
		avt.setIcon(new ImageIcon(MyApp.player.getAvatar()));
		pn.add(avt);
		
		String text = "<html><pre>Họ và Tên:\t<b>"+MyApp.player.getName() +"</b></pre></html>";  
		lblName = new JLabel(text);
		lblName.setFont(new java.awt.Font("Times New Roman", 0, 22));
		lblName.setBounds(9*u, (int)(2.5*u), 11*u, u);
		pn.add(lblName);
		
		text = "<html><pre>Giới tính:\t<b>"+MyApp.player.getSex() +"</b></pre></html>";  
		lblSex = new JLabel(text);
		lblSex.setFont(new java.awt.Font("Times New Roman", 0, 22));
		lblSex.setBounds(9*u, (int)(3.5*u), 11*u, u);
		pn.add(lblSex);
		
		if(level == 0){
			text = "<html><body>"
					+ "<div style=\"text-align: center; font-size: 20px; font-family: Times New Roman\">"
						+ "<b>Đã đạt trình độ dự bị lớp 1</b>"
					+ "<div></body></html>";
		}
		else{
			text = "<html><body>"
					+ "<div style=\"text-align: center; font-size: 25px; font-family: Times New Roman\">"
						+ "<b>Đã đạt trình độ lớp " + level + "</b>"
					+ "<div></body></html>";
		}
		
		epLevel = new JEditorPane();
		epLevel.setEditable(false);
		epLevel.setContentType("text/html");
		epLevel.setOpaque(false);
		epLevel.setBounds(9*u, (int)(4.5*u), 9*u, u);
		epLevel.setText(text);
		pn.add(epLevel);
		
		String d = null;
		switch(MyApp.difficulty){
		case MyApp.Difficulty.EASY:
			d = "Dễ";
			break;
		case MyApp.Difficulty.NORMAL:
			d = "Trung bình";
			break;
		case MyApp.Difficulty.HARD:
			d = "Khó";
			break;
		}
		text = "<html><pre>Cấp độ:\t\t<b>"+d+"</b></pre></html>";  
		lblDifficulty = new JLabel(text);
		lblDifficulty.setFont(new java.awt.Font("Times New Roman", 0, 22));
		lblDifficulty.setBounds(9*u, (int)(5.5*u), 11*u, u);
		pn.add(lblDifficulty);
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		text = "<html><pre>Ngày chơi:\t<b>"+dateFormat.format(date)+"</b></pre></html>";  
		lblDate = new JLabel(text);
		lblDate.setFont(new java.awt.Font("Times New Roman", 0, 22));
		lblDate.setBounds(9*u, (int)(6.5*u), 11*u, u);
		pn.add(lblDate);
		
		btnReplay = new JButton();
		btnReplay.setBounds(10*u, 15*u, 2*u, 2*u);
		ImageIcon icoOk = new ImageIcon("res/drawable/icon/replay.png");
		Image imgOk = icoOk.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnReplay.setIcon(new ImageIcon(imgOk));
		btnReplay.setOpaque(false);
		btnReplay.setContentAreaFilled(false);
		btnReplay.setBorderPainted(false);
		btnReplay.setToolTipText("Chơi lại");
		btnReplay.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				onReplay();
			}
		});
		panel.add(btnReplay);
		
		btnShare = new JButton();
		btnShare.setBounds(15*u, 15*u, 2*u, 2*u);
		icoOk = new ImageIcon("res/drawable/icon/share.png");
		imgOk = icoOk.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnShare.setIcon(new ImageIcon(imgOk));
		btnShare.setOpaque(false);
		btnShare.setContentAreaFilled(false);
		btnShare.setBorderPainted(false);
		btnShare.setToolTipText("Lưu kết quả chơi để chia sẻ");
		btnShare.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				onShare();
			}
		});
		panel.add(btnShare);
		
		btnQuit = new JButton();
		btnQuit.setBounds(20*u, 15*u, 2*u, 2*u);
		icoOk = new ImageIcon("res/drawable/icon/quit.png");
		imgOk = icoOk.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnQuit.setIcon(new ImageIcon(imgOk));
		btnQuit.setOpaque(false);
		btnQuit.setContentAreaFilled(false);
		btnQuit.setBorderPainted(false);
		btnQuit.setToolTipText("Thoát");
		btnQuit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});
		panel.add(btnQuit);
		
		// Frame
		this.setIconImage(MyApp.icon);
		this.setUndecorated(true);
		this.setSize(MyApp.ScreenSize);
		this.add(panel);

		pack();
	}
	
	private BufferedImage createImage(JPanel panel) {
	    int w = panel.getWidth();
	    int h = panel.getHeight();
	    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
	    Graphics2D g = bi.createGraphics();
	    panel.paint(g);
	    return bi;
	}
	
	private void onReplay(){
		this.setVisible(false);
		this.dispose();
		MyApp.restartGame();
	}
	
	private void onShare(){
		BufferedImage bi = createImage(this.pn);
        FileDialog fileDialog = new FileDialog(this, "Save", FileDialog.SAVE);
        fileDialog.setFilenameFilter(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".jpg");
            }
        });
        fileDialog.setFile("result.jpg");
        String filename = fileDialog.getFile();
        fileDialog.setVisible(true);
        if (fileDialog.getFile() == null) {
			return;
		}
        OutputStream out;
		try {
			out = new FileOutputStream(fileDialog.getDirectory() + filename);
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
	        encoder.encode(bi);
	        out.close();
		} catch (ImageFormatException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(Desktop.isDesktopSupported())
		{
		  try {
			Desktop.getDesktop().browse(new URI("http://www.facebook.com"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
	}
}
