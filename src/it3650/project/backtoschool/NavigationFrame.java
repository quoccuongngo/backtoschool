package it3650.project.backtoschool;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class NavigationFrame extends JFrame {
	private int u = MyApp.ScreenUnit;

	// Display components
	private JPanel panel;
	private JButton btnStart;
	private JButton btnSetting;
	private JButton btnAbout;
	private JButton btnExit;
	
	public NavigationFrame() {
		initComponents();
	}

	private void initComponents() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Panel
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/bkg_navigation.jpg");

			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(MyApp.ScreenSize);
		panel.setLayout(null);
		// end panel

		JLabel avt = new JLabel();
		avt.setBounds(1*u, 12*u, 5*u, 5*u);
		avt.setIcon(new ImageIcon(MyApp.player.getAvatar()));
		panel.add(avt);
		
		// btnStart
		btnStart = new JButton();
		btnStart.setBounds(9 * u, 7 * u, 7 * u, 2 * u);
		ImageIcon icoStart = new ImageIcon("res/drawable/icon/play-game.png");
		Image imgStart = icoStart.getImage().getScaledInstance(7 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnStart.setIcon(new ImageIcon(imgStart));
		btnStart.setOpaque(false);
		btnStart.setContentAreaFilled(false);
		btnStart.setBorderPainted(false);
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				onStart(evt);
			}
		});
		panel.add(btnStart);
		// btnStart

		// btnSetting
		btnSetting = new JButton();
		btnSetting.setBounds((int)(10.2*u), (int)(9.5*u), 7 * u, 2 * u);
		ImageIcon icoSetting = new ImageIcon("res/drawable/icon/setting.png");
		Image imgSetting = icoSetting.getImage().getScaledInstance(7 * u,
				2 * u, Image.SCALE_SMOOTH);
		btnSetting.setIcon(new ImageIcon(imgSetting));
		btnSetting.setOpaque(false);
		btnSetting.setContentAreaFilled(false);
		btnSetting.setBorderPainted(false);
		btnSetting.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				onPreference();
			}
		});
		panel.add(btnSetting);
		// btnSetting

		// btnAbout
		btnAbout = new JButton();
		btnAbout.setBounds((int)(11.4*u), 12 * u, 7 * u, 2 * u);
		ImageIcon icoInstruction = new ImageIcon("res/drawable/icon/about.png");
		Image imgInstruction = icoInstruction.getImage().getScaledInstance(
				7 * u, 2 * u, Image.SCALE_SMOOTH);
		btnAbout.setIcon(new ImageIcon(imgInstruction));
		btnAbout.setOpaque(false);
		btnAbout.setContentAreaFilled(false);
		btnAbout.setBorderPainted(false);
		btnAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				onAbout(evt);
			}
		});
		panel.add(btnAbout);
		// end btnAbout

		// btnExit
		btnExit = new JButton();
		btnExit.setBounds((int)(12.6*u), (int)(14.5*u), 7 * u, 2 * u);
		ImageIcon icoExit = new ImageIcon("res/drawable/icon/exit-game.png");
		Image imgExit = icoExit.getImage().getScaledInstance(7 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnExit.setIcon(new ImageIcon(imgExit));
		btnExit.setOpaque(false);
		btnExit.setContentAreaFilled(false);
		btnExit.setBorderPainted(false);
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});
		panel.add(btnExit);
		// end btnExit
		
		// Frame
		this.setIconImage(MyApp.icon);
		this.setUndecorated(true);
		this.setSize(MyApp.ScreenSize);
		this.add(panel);

		pack();

	}

	@SuppressWarnings("deprecation")
	private void onStart(ActionEvent evt) {
		MyApp.sound.suspend();
		MyApp.frmPlaying = new PlayingFrame();
		MyApp.frmPlaying.setVisible(true);
		this.setVisible(false);
		this.dispose();
	}

	private void onPreference() {
		PreferenceDialog pfr = new PreferenceDialog();
		pfr.appear();
	}

	private void onAbout(ActionEvent evt) {
		AboutFrame frmAbout = new AboutFrame();
		frmAbout.setVisible(true);
		this.setVisible(false);
	}
}

@SuppressWarnings("serial")
class PreferenceDialog extends JDialog {
	private int u = MyApp.ScreenUnit;
	private int dif = MyApp.difficulty;
	private JPanel panel;
	private JLabel icon;
	private JButton btnEasy;
	private JButton btnMedium;
	private JButton btnHard;
	private JButton btnOK;
	private JLabel lblEasy;
	private JLabel lblMedium;
	private JLabel lblHard;

	PreferenceDialog() {
		super(MyApp.frmNavigation, ModalityType.APPLICATION_MODAL);
		initComponents();		
		switch (MyApp.difficulty) {
		case MyApp.Difficulty.EASY:
			lblEasy.setFont(new java.awt.Font("Times New Roman", 1, 30));
			lblMedium.setFont(new java.awt.Font("Times New Roman", 0, 12));
			lblHard.setFont(new java.awt.Font("Times New Roman", 0, 12));
			break;
		case MyApp.Difficulty.NORMAL:
			lblEasy.setFont(new java.awt.Font("Times New Roman", 0, 12));
			lblMedium.setFont(new java.awt.Font("Times New Roman", 1, 30));
			lblHard.setFont(new java.awt.Font("Times New Roman", 0, 12));
			break;
		case MyApp.Difficulty.HARD:
			lblEasy.setFont(new java.awt.Font("Times New Roman", 0, 12));
			lblMedium.setFont(new java.awt.Font("Times New Roman", 0, 12));
			lblHard.setFont(new java.awt.Font("Times New Roman", 1, 30));
			break;
		}
	}

	private void initComponents() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/bkg_preference.jpg");
			
			@Override
			public void paintComponent(Graphics g){
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(new Dimension(10*u, MyApp.ScreenSize.height));
		panel.setLayout(null);
		//end panel
		
		icon =  new JLabel();
		icon.setBounds(u, u, 8*u, 4*u);
		ImageIcon ico = new ImageIcon("res/drawable/icon/icon_large.png");
		Image img = ico.getImage().getScaledInstance(8*u, 4*u,
				Image.SCALE_SMOOTH);
		icon.setIcon(new ImageIcon(img));
		panel.add(icon);
		
		btnEasy = new JButton();
		btnEasy.setBounds(3*u, 5*u, 5*u/2, 5*u/2);
		ico = new ImageIcon("res/drawable/icon/easy.png");
		img = ico.getImage().getScaledInstance(5*u/2, 5*u/2,
				Image.SCALE_SMOOTH);
		btnEasy.setIcon(new ImageIcon(img));
		btnEasy.setOpaque(false);
		btnEasy.setContentAreaFilled(false);
		btnEasy.setBorderPainted(false);
		btnEasy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				dif = MyApp.Difficulty.EASY;
				lblEasy.setFont(new java.awt.Font("Times New Roman", 1, 30));
				lblMedium.setFont(new java.awt.Font("Times New Roman", 0, 12));
				lblHard.setFont(new java.awt.Font("Times New Roman", 0, 12));
			}
		});
		panel.add(btnEasy);
		
		lblEasy = new JLabel("Dễ");
		lblEasy.setBounds(6*u, 6*u, 4*u, u);
		lblEasy.setForeground(Color.white);
		panel.add(lblEasy);
		
		btnMedium = new JButton();
		btnMedium.setBounds(3*u, 8*u, 5*u/2, 5*u/2);
		ico = new ImageIcon("res/drawable/icon/medium.png");
		img = ico.getImage().getScaledInstance(5*u/2, 5*u/2,
				Image.SCALE_SMOOTH);
		btnMedium.setIcon(new ImageIcon(img));
		btnMedium.setOpaque(false);
		btnMedium.setContentAreaFilled(false);
		btnMedium.setBorderPainted(false);
		btnMedium.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				dif = MyApp.Difficulty.NORMAL;
				lblEasy.setFont(new java.awt.Font("Times New Roman", 0, 12));
				lblMedium.setFont(new java.awt.Font("Times New Roman", 1, 30));
				lblHard.setFont(new java.awt.Font("Times New Roman", 0, 12));
			}
		});
		panel.add(btnMedium);
		
		lblMedium = new JLabel("Trung bình");
		lblMedium.setBounds(6*u, 9*u, 4*u, u);
		lblMedium.setForeground(Color.white);
		panel.add(lblMedium);
		
		btnHard = new JButton();
		btnHard.setBounds(3*u, 11*u, 5*u/2, 5*u/2);
		ico = new ImageIcon("res/drawable/icon/hard.png");
		img = ico.getImage().getScaledInstance(5*u/2, 5*u/2,
				Image.SCALE_SMOOTH);
		btnHard.setIcon(new ImageIcon(img));
		btnHard.setOpaque(false);
		btnHard.setContentAreaFilled(false);
		btnHard.setBorderPainted(false);
		btnHard.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				dif = MyApp.Difficulty.HARD;
				lblEasy.setFont(new java.awt.Font("Times New Roman", 0, 12));
				lblMedium.setFont(new java.awt.Font("Times New Roman", 0, 12));
				lblHard.setFont(new java.awt.Font("Times New Roman", 1, 30));
			}
		});
		panel.add(btnHard);
		
		lblHard = new JLabel("Khó");
		lblHard.setBounds(6*u, 12*u, 4*u, u);
		lblHard.setForeground(Color.white);
		panel.add(lblHard);
		
		btnOK = new JButton();
		btnOK.setBounds(3*u, 14*u, 4*u, 3*u);
		ico = new ImageIcon("res/drawable/icon/ok.png");
		img = ico.getImage().getScaledInstance(4*u, 3*u,
				Image.SCALE_SMOOTH);
		btnOK.setIcon(new ImageIcon(img));
		btnOK.setOpaque(false);
		btnOK.setContentAreaFilled(false);
		btnOK.setBorderPainted(false);
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				MyApp.difficulty = dif;
				PreferenceDialog.this.disappear();
			}
		});
		panel.add(btnOK);
		
		this.setLocation(MyApp.ScreenSize.width, 0);
		this.setSize(panel.getSize());
		this.add(panel);
		this.setResizable(false);
		this.setUndecorated(true);
		pack();
	}
	
	public void appear() {
		final Timer timer = new Timer(15, null);
        timer.setRepeats(true);
        timer.addActionListener(new ActionListener() {
            private int x = MyApp.ScreenSize.width;
            private int b = x - 10*u;
            @Override public void actionPerformed(ActionEvent e) {
            	x -= 5;
                PreferenceDialog.this.setLocation(x, 0);
                if (x <= b) {
                	timer.stop();
                }
            }
        });
        timer.start();
        this.setVisible(true);
	}
	public void disappear(){
		int x = this.getLocation().x;
		do {
			x += 10;
			this.setLocation(x, 0);
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while (x < MyApp.ScreenSize.width);
		this.setVisible(false);
		this.dispose();
	}
}