package it3650.project.backtoschool;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.plaf.basic.BasicProgressBarUI;

@SuppressWarnings("serial")
public class PlayingFrame extends JFrame {
	final static int TIMEUP = 100;
	
	private int u = MyApp.ScreenUnit;
	private Question question;
	private int question_no;
	private int time;
	private Timer timer;

	// Display components
	private JPanel panel;
	private ImageIcon background;
	private JProgressBar prbTime;
	private JEditorPane epQuestion;
	private JButton btnA;
	private JButton btnB;
	private JButton btnC;
	private JButton btnD;
	private JButton btnAnswer;
	
	private JButton btnHelp1; // 50:50
	private boolean bHelp1 = false;
	private JButton btnHelp2; // Gọi điện thoại
	private JButton btnHelp3; // Đổi câu hỏi
	private JButton btnHelp4; // Hỏi khán giả
	private JButton btnStop; // Dừng cuộc chơi
	
	public PlayingFrame() {
		switch (MyApp.difficulty) {
		case MyApp.Difficulty.EASY:
			time = 60;
			break;
		case MyApp.Difficulty.NORMAL:
			time = 30;
			break;
		case MyApp.Difficulty.HARD:
			time = 15;
			break;
		}

		initComponents();
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int value = PlayingFrame.this.prbTime.getValue();
				PlayingFrame.this.prbTime.setValue(--value);
				if (value == 0) {
					gameOver(TIMEUP);
				}
			}
		});
		updateContents(MyApp.level);
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Panel
		background = new ImageIcon("res/drawable/background/bkg_playing1.jpg");
		panel = new JPanel() {
			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(background.getImage(), 0, 0, d.width, d.height,
						null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(MyApp.ScreenSize);
		panel.setLayout(null);
		// end panel

		JLabel avt = new JLabel();
		avt.setBounds(1*u, 12*u, 5*u, 5*u);
		avt.setIcon(new ImageIcon(MyApp.player.getAvatar()));
		panel.add(avt);
		
		// prbTime
		prbTime = new JProgressBar();
		prbTime.setStringPainted(true);
		prbTime.setBounds((int)(12*u), u, 14 * u, u);
		prbTime.setForeground(new Color(0, 220, 0));
		prbTime.setUI(new BasicProgressBarUI() {
			protected Color getSelectionBackground() {
				return Color.black;
			}

			protected Color getSelectionForeground() {
				return Color.red;
			}
		});
		prbTime.setMaximum(time);
		prbTime.setMinimum(0);
		panel.add(prbTime);
		// end prbTime

		// Question
		epQuestion = new JEditorPane();
		epQuestion.setBounds((int)(12*u), 2 * u, 14 * u, 6 * u);
		epQuestion.setMargin(new java.awt.Insets(30, 30, 30, 30));
		epQuestion.setEditable(false);
		epQuestion.setContentType("text/html");
		panel.add(epQuestion);
		// end Question

		// btnA
		btnA = new JButton();
		btnA.setBounds((int)(13.5*u), 11 * u, 11 * u, u);
		btnA.setForeground(Color.WHITE);
		btnA.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnAnswer = btnA;
				answer("A");
			}
		});
		btnA.setFocusPainted(false);
		panel.add(btnA);
		// end btnA

		// btnB
		btnB = new JButton();
		btnB.setBounds((int)(13.5*u), (int)(12.6*u), 11 * u, u);
		btnB.setForeground(Color.WHITE);
		btnB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnAnswer = btnB;
				answer("B");
			}
		});
		btnB.setFocusPainted(false);
		panel.add(btnB);
		// end btnA

		// btnC
		btnC = new JButton();
		btnC.setBounds((int)(13.5*u), (int)(14.2*u), 11 * u, u);
		btnC.setForeground(Color.WHITE);
		btnC.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnAnswer = btnC;
				answer("C");
			}
		});
		btnC.setFocusPainted(false);
		panel.add(btnC);
		// end btnC

		// btnD
		btnD = new JButton();
		btnD.setBounds((int)(13.5*u), (int)(15.8*u), 11 * u, u);
		btnD.setForeground(Color.WHITE);
		btnD.setVerticalTextPosition(SwingConstants.CENTER);
		btnD.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnAnswer = btnD;
				answer("D");
			}
		});
		btnD.setFocusPainted(false);
		panel.add(btnD);
		// end btnD

		// btnHelp1
		btnHelp1 = new JButton();
		btnHelp1.setBounds(14 * u, (int)(8.5*u), 2 * u, 2 * u);
		ImageIcon icoHelp1 = new ImageIcon("res/drawable/icon/help1.png");
		Image imgHelp1 = icoHelp1.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnHelp1.setIcon(new ImageIcon(imgHelp1));
		btnHelp1.setOpaque(false);
		btnHelp1.setContentAreaFilled(false);
		btnHelp1.setBorderPainted(false);
		btnHelp1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				useHelp(1);
			}
		});
		panel.add(btnHelp1);
		// end btnHelp1

		// btnHelp2
		btnHelp2 = new JButton();
		btnHelp2.setBounds(16 * u, (int)(8.5*u), 2 * u, 2 * u);
		ImageIcon icoHelp2 = new ImageIcon("res/drawable/icon/help2.png");
		Image imgHelp2 = icoHelp2.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnHelp2.setIcon(new ImageIcon(imgHelp2));
		btnHelp2.setOpaque(false);
		btnHelp2.setContentAreaFilled(false);
		btnHelp2.setBorderPainted(false);
		btnHelp2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				useHelp(2);
			}
		});
		panel.add(btnHelp2);
		// end btnHelp1

		// btnHelp3
		btnHelp3 = new JButton();
		btnHelp3.setBounds(18 * u, (int)(8.5*u), 2 * u, 2 * u);
		ImageIcon icoHelp3 = new ImageIcon("res/drawable/icon/help3.png");
		Image imgHelp3 = icoHelp3.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnHelp3.setIcon(new ImageIcon(imgHelp3));
		btnHelp3.setOpaque(false);
		btnHelp3.setContentAreaFilled(false);
		btnHelp3.setBorderPainted(false);
		btnHelp3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				useHelp(3);
			}
		});
		panel.add(btnHelp3);
		// end btnHelp3

		// btnHelp4
		btnHelp4 = new JButton();
		btnHelp4.setBounds(20 * u, (int)(8.5*u), 2 * u, 2 * u);
		ImageIcon icoHelp4 = new ImageIcon("res/drawable/icon/help4.png");
		Image imgHelp4 = icoHelp4.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnHelp4.setIcon(new ImageIcon(imgHelp4));
		btnHelp4.setOpaque(false);
		btnHelp4.setContentAreaFilled(false);
		btnHelp4.setBorderPainted(false);
		btnHelp4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				useHelp(4);
			}
		});
		panel.add(btnHelp4);
		// end btnHelp1

		// btnStop
		btnStop = new JButton();
		btnStop.setBounds(22 * u, (int)(8.5*u), 2 * u, 2 * u);
		ImageIcon icoStop = new ImageIcon("res/drawable/icon/stop.png");
		Image imgStop = icoStop.getImage().getScaledInstance(2 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnStop.setIcon(new ImageIcon(imgStop));
		btnStop.setOpaque(false);
		btnStop.setContentAreaFilled(false);
		btnStop.setBorderPainted(false);
		btnStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				stopGame();
			}
		});
		panel.add(btnStop);
		// end btnStop

		// Frame
		this.setIconImage(MyApp.icon);
		this.setUndecorated(true);
		this.setSize(MyApp.ScreenSize);
		this.add(panel);

		pack();
	}

	private void answer(String Ans) {
		this.btnAnswer.setBackground(new Color(0x0a85ee));
		this.timer.stop();
		try {
			Thread.sleep(new Random().nextInt(3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		String correct = question.getCorrectAns();
		showCorrectAnswer(correct);
		if (!Ans.equals(correct)) {
			this.btnAnswer.setBackground(new Color(0xee0a0a));
			new Thread(new Runnable() {
				@Override
				public void run() {
					AePlayWave sound = new AePlayWave("res/raw/false.wav");
					sound.start();
					while(sound.isAlive());
					gameOver(MyApp.level);
				}
			}).start();
			return;
		}
		MyApp.level++;
		new Thread(new Runnable() {
			@Override
			public void run() {
				AePlayWave sound = new AePlayWave("res/raw/true.wav");
				sound.start();
				while(sound.isAlive());
				if (MyApp.level == 12) {
					gameOver(12);
					return;
				}
				updateContents(MyApp.level);
			}
		}).start();
	}

	private void showCorrectAnswer(String correct){
		switch (correct) {
		case "A":
			this.btnA.setBackground(new Color(0x0aee7b));
			break;
		case "B":
			this.btnB.setBackground(new Color(0x0aee7b));
			break;
		case "C":
			this.btnC.setBackground(new Color(0x0aee7b));
			break;
		case "D":
			this.btnD.setBackground(new Color(0x0aee7b));
			break;
		}
	}
	private void useHelp(int help) {
		switch (help) {
		// 50 50
		case 1:
			bHelp1 = true;
			btnHelp1.setEnabled(false);
			String s = question.getHelp1();
			if (s.equals("AB")) {
				btnC.setVisible(false);
				btnD.setVisible(false);
			} else if (s.equals("AC")) {
				btnB.setVisible(false);
				btnD.setVisible(false);
			} else if (s.equals("AD")) {
				btnB.setVisible(false);
				btnC.setVisible(false);
			} else if (s.equals("BC")) {
				btnA.setVisible(false);
				btnD.setVisible(false);
			} else if (s.equals("BD")) {
				btnA.setVisible(false);
				btnC.setVisible(false);
			} else if (s.equals("CD")) {
				btnA.setVisible(false);
				btnB.setVisible(false);
			}
			break;
		// Gọi điện thoại
		case 2:
			timer.stop();
			String help2 = question.getHelp2();
			if (bHelp1) {
				StringBuilder temp = new StringBuilder();
				String help1 = question.getHelp1();
				Random r = new Random();
				for (int i = 0; i < help2.length(); i++) {
					if (help1.indexOf(help2.charAt(i)) == -1) {
						temp.append(help1.charAt(r.nextInt(2)));
					}
					else {
						temp.append(help2.charAt(i));
					}
				}
				help2 = temp.toString();
			}
			Help2Dialog dlg2 = new Help2Dialog(help2);
			dlg2.setVisible(true);
			btnHelp2.setEnabled(false);
			timer.start();
			break;
		// Đổi câu hỏi
		case 3:
			if (new ConfirmDialog(1).showDialogForResult() == ConfirmDialog.OK) {
				timer.stop();
				btnHelp3.setEnabled(false);
				updateContents(MyApp.level);
			}
			break;
		// Hỏi khán giả
		case 4:
			timer.stop();
			Help4Dialog dlg4 = new Help4Dialog(MyApp.level,
					question.getCorrectAns(), this.bHelp1, question.getHelp1());
			dlg4.setVisible(true);
			btnHelp4.setEnabled(false);
			timer.start();
			break;
		}
	}

	private void stopGame() {
		if (new ConfirmDialog(2).showDialogForResult() == ConfirmDialog.OK) {
			timer.stop();
			gameOver(MyApp.level);
		}
	}

	private void gameOver(int level) {
		timer.stop();
		if (level == 12) {
			new NotificationDialog(12).setVisible(true);
		} else if (level == TIMEUP) {
			timer.stop();
			JOptionPane.showMessageDialog(null, "Hết giờ", "KẾT THÚC",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			new NotificationDialog(MyApp.level).setVisible(true);
		}
		new GameOverFrame(MyApp.level).setVisible(true);
		this.setVisible(false);
		this.dispose();
	}

	private void updateContents(int level) {
		if (level == 5) {
			new PassLevelDialog(1).setVisible(true);
			background = new ImageIcon("res/drawable/background/bkg_playing2.jpg");
			panel.paintComponents(this.getGraphics());
			panel.repaint();
		} else if (level == 9) {
			new PassLevelDialog(2).setVisible(true);
			background = new ImageIcon("res/drawable/background/bkg_playing3.jpg");
			panel.paintComponents(this.getGraphics());
			panel.repaint();
		}
		
		new LevelDialog(level).appear();
		// Get question
		this.bHelp1 = false; //Khi tạo một câu hỏi mới thì 50 50 chưa được sử dụng
		question = getQuestion(level);

		// prbTime
		prbTime.setString("Câu hỏi lớp " + (level + 1));

		// Question
		epQuestion.setText("<html><body><div style=\"text-align: center; font-size: 24px; font-family: Times New Roman\">"
				+ question.getQues() + "<div></body></html>");
		btnA.setVisible(true);
		btnA.setText(question.getAnsA());
		btnB.setVisible(true);
		btnB.setText(question.getAnsB());
		btnC.setVisible(true);
		btnC.setText(question.getAnsC());
		btnD.setVisible(true);
		btnD.setText(question.getAnsD());

		btnA.setBackground(new Color(0x2c7514));
		btnB.setBackground(new Color(0x2c7514));
		btnC.setBackground(new Color(0x2c7514));
		btnD.setBackground(new Color(0x2c7514));
		
		this.prbTime.setValue(time);
		timer.restart();
	}

	private Question getQuestion(int level) {
		int no;
		Random generator = new Random();
		do {
			no = generator.nextInt(MyApp.QuestionCollection.get(level).size());
		} while (no == question_no);
		question_no = no;
		return MyApp.QuestionCollection.get(level).get(question_no);
	}
	
	private class PassLevelDialog extends JDialog {
		private int u = MyApp.ScreenUnit;
		private JPanel panel;
		private JButton btnOK;
		private AePlayWave sound;
		
		public PassLevelDialog(int lv) {
			super(MyApp.frmPlaying, ModalityType.APPLICATION_MODAL);
			sound =  new AePlayWave("res/raw/pass" + lv + ".wav");
			initComponents(lv);
		}
		
		private void initComponents(int lv) {
			String file_bkg = "res/drawable/background/pl"+lv+".jpg";
			panel = new JPanel() {
				ImageIcon icon = new ImageIcon(file_bkg);
				
				@Override
				public void paintComponent(Graphics g){
					Dimension d = getSize();
					g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
					setOpaque(false);
					super.paintComponent(g);
				}
			};
			panel.setPreferredSize(MyApp.ScreenSize);
			panel.setLayout(null);
			
			btnOK = new JButton();
			btnOK.setBounds(14*u, 13*u, 4*u, 4*u);
			ImageIcon ico = new ImageIcon("res/drawable/icon/continue.png");
			Image img = ico.getImage().getScaledInstance(4*u, 4*u,
					Image.SCALE_SMOOTH);
			btnOK.setIcon(new ImageIcon(img));
			btnOK.setOpaque(false);
			btnOK.setContentAreaFilled(false);
			btnOK.setBorderPainted(false);
			btnOK.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					PassLevelDialog.this.setVisible(false);
					PassLevelDialog.this.dispose();
				}
			});
			panel.add(btnOK);
			
			this.setSize(panel.getSize());
			this.add(panel);
			this.setResizable(false);
			this.setUndecorated(true);
			
			pack();
			this.setLocationRelativeTo(null);
		}
		
		@SuppressWarnings("deprecation")
		@Override
		public void setVisible(boolean b) {
			if (b) {
				sound.start();
				super.setVisible(true);
			} else {
				sound.stop();
				super.setVisible(false);
			}
		}
	}
	
	private class ConfirmDialog extends JDialog {
		final static int OK = 0;
		final static int CANCEL = 1;
		private int result;
		
		private int u = MyApp.ScreenUnit;
		private JPanel panel;
		private JButton btnOK;
		private JButton btnCancel;
		
		public ConfirmDialog(int code) {
			super(MyApp.frmPlaying, ModalityType.APPLICATION_MODAL);
			initComponents(code);
		}
		
		private void initComponents(int code) {
			String file_bkg = "res/drawable/background/confirm"+code+".png";
			panel = new JPanel() {
				ImageIcon icon = new ImageIcon(file_bkg);
				
				@Override
				public void paintComponent(Graphics g){
					Dimension d = getSize();
					g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
					setOpaque(false);
					super.paintComponent(g);
				}
			};
			panel.setPreferredSize(new Dimension(20*u, 14*u));
			panel.setLayout(null);
			
			btnOK = new JButton();
			btnOK.setBounds(10*u, 7*u, 2*u, 2*u);
			ImageIcon ico = new ImageIcon("res/drawable/icon/ok.png");
			Image img = ico.getImage().getScaledInstance(2*u, 2*u,
					Image.SCALE_SMOOTH);
			btnOK.setIcon(new ImageIcon(img));
			btnOK.setOpaque(false);
			btnOK.setContentAreaFilled(false);
			btnOK.setBorderPainted(false);
			btnOK.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					result = OK;
					ConfirmDialog.this.setVisible(false);
					ConfirmDialog.this.dispose();
				}
			});
			panel.add(btnOK);
			
			btnCancel = new JButton();
			btnCancel.setBounds(14*u, 7*u, 2*u, 2*u);
			ico = new ImageIcon("res/drawable/icon/cancel.png");
			img = ico.getImage().getScaledInstance(2*u, 2*u,
					Image.SCALE_SMOOTH);
			btnCancel.setIcon(new ImageIcon(img));
			btnCancel.setOpaque(false);
			btnCancel.setContentAreaFilled(false);
			btnCancel.setBorderPainted(false);
			btnCancel.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					result = CANCEL;
					ConfirmDialog.this.setVisible(false);
					ConfirmDialog.this.dispose();
				}
			});
			panel.add(btnCancel);
			
			this.setSize(panel.getSize());
			this.add(panel);
			this.setResizable(false);
			this.setUndecorated(true);
			
			pack();
			this.setLocationRelativeTo(null);
		}
		int showDialogForResult() {
			setVisible(true);
			return result;
		}
	}
	
	private class NotificationDialog extends JDialog {
		private int u = MyApp.ScreenUnit;
		private JPanel panel;
		private JButton btnOK;
		private String file_bkg;
		private AePlayWave sound;
		
		public NotificationDialog(int lv) {
			super(MyApp.frmPlaying, ModalityType.APPLICATION_MODAL);
			if (lv == 12) {
				file_bkg = "res/drawable/background/graduation.jpg";
				sound = new AePlayWave("res/raw/bravo.wav");
			}
			else {
				file_bkg = "res/drawable/background/gameover.jpg";
				sound = new AePlayWave("res/raw/failed.wav");
			}
			initComponents(lv);
		}
		
		private void initComponents(int lv) {
			panel = new JPanel() {
				ImageIcon icon = new ImageIcon(file_bkg);
				
				@Override
				public void paintComponent(Graphics g){
					Dimension d = getSize();
					g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
					setOpaque(false);
					super.paintComponent(g);
				}
			};
			panel.setPreferredSize(MyApp.ScreenSize);
			panel.setLayout(null);
			
			btnOK = new JButton();
			btnOK.setBounds(14*u, 13*u, 4*u, 4*u);
			ImageIcon ico = new ImageIcon("res/drawable/icon/next.png");
			Image img = ico.getImage().getScaledInstance(4*u, 4*u,
					Image.SCALE_SMOOTH);
			btnOK.setIcon(new ImageIcon(img));
			btnOK.setOpaque(false);
			btnOK.setContentAreaFilled(false);
			btnOK.setBorderPainted(false);
			btnOK.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					NotificationDialog.this.setVisible(false);
					NotificationDialog.this.dispose();
				}
			});
			panel.add(btnOK);
			
			this.setSize(panel.getSize());
			this.add(panel);
			this.setResizable(false);
			this.setUndecorated(true);
			
			pack();
			this.setLocationRelativeTo(null);
		}
		
		@Override
		public void setVisible(boolean b) {
			if (b) {
				sound.start();
				super.setVisible(true);
			}
			else {
				super.setVisible(false);
			}
		}
	}
}

@SuppressWarnings("serial")
class Help2Dialog extends JDialog {
	private int u = MyApp.ScreenUnit;
	private String help;
	private JPanel panel;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btnClose;
	private JEditorPane editorPane;
	
	Help2Dialog(String help) {
		super(MyApp.frmPlaying, ModalityType.APPLICATION_MODAL);
		this.help = help;
		initComponents();
	}
	
	private void initComponents() {
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/bkg_help2.jpg");
			
			@Override
			public void paintComponent(Graphics g){
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(new Dimension(24*u, 14*u));
		panel.setLayout(null);
		
		btn1 = new JButton();
		btn1.setBounds(u, 3*u, 4*u, 4*u);
		ImageIcon ico = new ImageIcon("res/drawable/help2/doctor.jpg");
		Image img = ico.getImage().getScaledInstance(4*u, 4*u,
				Image.SCALE_SMOOTH);
		btn1.setIcon(new ImageIcon(img));
		btn1.setToolTipText("Bác sĩ");
		btn1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				help2(0);
				btn2.setEnabled(false);
				btn3.setEnabled(false);
				btn4.setEnabled(false);
			}
		});
		panel.add(btn1);
		
		btn2 = new JButton();
		btn2.setBounds(7*u, 3*u, 4*u, 4*u);
		ico = new ImageIcon("res/drawable/help2/engineer.jpg");
		img = ico.getImage().getScaledInstance(4*u, 4*u,
				Image.SCALE_SMOOTH);
		btn2.setIcon(new ImageIcon(img));
		btn2.setToolTipText("Kỹ sư");
		btn2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				help2(1);
				btn1.setEnabled(false);
				btn3.setEnabled(false);
				btn4.setEnabled(false);
			}
		});
		panel.add(btn2);
		
		btn3 = new JButton();
		btn3.setBounds(13*u, 3*u, 4*u, 4*u);
		ico = new ImageIcon("res/drawable/help2/journalist.jpg");
		img = ico.getImage().getScaledInstance(4*u, 4*u,
				Image.SCALE_SMOOTH);
		btn3.setIcon(new ImageIcon(img));
		btn3.setToolTipText("Phóng viên");
		btn3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				help2(2);
				btn1.setEnabled(false);
				btn2.setEnabled(false);
				btn4.setEnabled(false);
			}
		});
		panel.add(btn3);
		
		btn4 = new JButton();
		btn4.setBounds(19*u, 3*u, 4*u, 4*u);
		ico = new ImageIcon("res/drawable/help2/teacher.jpg");
		img = ico.getImage().getScaledInstance(4*u, 4*u,
				Image.SCALE_SMOOTH);
		btn4.setIcon(new ImageIcon(img));
		btn4.setToolTipText("Giáo viên");
		btn4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				help2(3);
				btn1.setEnabled(false);
				btn2.setEnabled(false);
				btn3.setEnabled(false);
			}
		});
		panel.add(btn4);
		
		btnClose = new JButton();
		btnClose.setBounds((int)(21.5*u), u, (int)(1.5*u), (int)(1.5*u));
		ico = new ImageIcon("res/drawable/icon/close.png");
		img = ico.getImage().getScaledInstance((int)(1.5*u), (int)(1.5*u),
				Image.SCALE_SMOOTH);
		btnClose.setIcon(new ImageIcon(img));
		btnClose.setOpaque(false);
		btnClose.setContentAreaFilled(false);
		btnClose.setBorderPainted(false);
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				Help2Dialog.this.dispose();
			}
		});
		panel.add(btnClose);
		
		editorPane = new JEditorPane();
		editorPane.setBounds(7*u, 10*u, 10*u, 2*u);
		editorPane.setEditable(false);
		editorPane.setContentType("text/html");
		editorPane.setOpaque(false);
		panel.add(editorPane);
		
		
		this.setSize(panel.getSize());
		this.add(panel);
		this.setResizable(false);
		this.setUndecorated(true);
		
		pack();
		this.setLocationRelativeTo(null);
	}
	
	private void help2(int index) {
		editorPane.setText("<html><body><div style=\"text-align: center; font-size: 32px; color: white; font-family: Times New Roman\">"
				+ "Theo tôi là đáp án " + this.help.charAt(index) 
				+ "<div></body></html>");
	}
}

@SuppressWarnings("serial")
class Help4Dialog extends JDialog {
	private int u = MyApp.ScreenUnit;
	private JPanel panel;
	private JProgressBar prbA;
	private JProgressBar prbB;
	private JProgressBar prbC;
	private JProgressBar prbD;
	private JButton btnClose;

	Help4Dialog(int level, String corectAns, boolean bHelp1, String help1) {
		super(MyApp.frmPlaying, ModalityType.APPLICATION_MODAL);//, "help 4", true);
		initComponents();		
		setValue(level, corectAns, bHelp1, help1);
	}

	private void initComponents() {
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/bkg_help4.jpg");
			
			@Override
			public void paintComponent(Graphics g){
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(new Dimension(20*u, 12*u));
		panel.setLayout(null);
		//end panel
		
		prbA = new JProgressBar();
		prbA.setBounds(5*u, 3*u, 10*u, 1*u);
		prbA.setMaximum(100);
		prbA.setMinimum(0);
		prbA.setStringPainted(true);
		panel.add(prbA);
		
		prbB = new JProgressBar();
		prbB.setBounds(5*u, 5*u, 10*u, 1*u);
		prbB.setMaximum(100);
		prbB.setMinimum(0);
		prbB.setStringPainted(true);
		panel.add(prbB);
		
		prbC = new JProgressBar();
		prbC.setBounds(5*u, 7*u, 10*u, 1*u);
		prbC.setMaximum(100);
		prbC.setMinimum(0);
		prbC.setStringPainted(true);
		panel.add(prbC);
		
		prbD = new JProgressBar();
		prbD.setBounds(5*u, 9*u, 10*u, 1*u);
		prbD.setMaximum(100);
		prbD.setMinimum(0);
		prbD.setStringPainted(true);
		panel.add(prbD);
		
		btnClose = new JButton();
		btnClose.setBounds((int)(17.5*u), u, (int)(1.5*u), (int)(1.5*u));
		ImageIcon ico = new ImageIcon("res/drawable/icon/close.png");
		Image img = ico.getImage().getScaledInstance((int)(1.5*u), (int)(1.5*u),
				Image.SCALE_SMOOTH);
		btnClose.setIcon(new ImageIcon(img));
		btnClose.setOpaque(false);
		btnClose.setContentAreaFilled(false);
		btnClose.setBorderPainted(false);
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				Help4Dialog.this.dispose();
			}
		});
		panel.add(btnClose);
		this.setSize(panel.getSize());
		this.add(panel);
		this.setResizable(false);
		this.setUndecorated(true);
		
		pack();
		this.setLocationRelativeTo(null);
	}

	private void setValue(int level, String corectAns, boolean bHelp1,
			String help1) {

		// chưa sử dụng 50 50
		if (!bHelp1) {
			// generate value
			int a, b, c, d;
			Random generator = new Random();
			// grade 1, 2, 3
			if (level < 3) {
				a = generator.nextInt(100 - 80 + 1) + 80;
				b = generator.nextInt(100 - a + 1);
				c = generator.nextInt(100 - a - b + 1);
				d = 100 - a - b - c;
			}
			// grade 4, 5, 6
			else if (level < 6) {
				a = generator.nextInt(100 - 60 + 1) + 60;
				b = generator.nextInt(100 - a + 1);
				c = generator.nextInt(100 - a - b + 1);
				d = 100 - a - b - c;
			}
			// grade 7, 8, 9
			else if (level < 9) {
				a = generator.nextInt(100 - 40 + 1) + 40;
				b = generator.nextInt(100 - a + 1);
				c = generator.nextInt(100 - a - b + 1);
				d = 100 - a - b - c;
			} else {
				a = generator.nextInt(100 - 20 + 1) + 20;
				b = generator.nextInt(100 - a + 1);
				c = generator.nextInt(100 - a - b + 1);
				d = 100 - a - b - c;
			}

			// set value
			switch (corectAns) {
			case "A":
				prbA.setValue(a);
				prbB.setValue(b);
				prbC.setValue(c);
				prbD.setValue(d);
				break;
			case "B":
				prbB.setValue(a);
				prbA.setValue(b);
				prbC.setValue(c);
				prbD.setValue(d);
				break;
			case "C":
				prbC.setValue(a);
				prbB.setValue(b);
				prbA.setValue(c);
				prbD.setValue(d);
				break;
			case "D":
				prbD.setValue(a);
				prbB.setValue(b);
				prbC.setValue(c);
				prbA.setValue(d);
				break;
			}
		}
		// Đã sử dụng 50 50
		else {
			int a, b;
			Random generator = new Random();
			// grade 1 2 3
			if (level < 3) {
				a = generator.nextInt(100 - 80 + 1) + 80;
				b = 100 - a;
			}
			// grade 4 5 6
			else if (level < 6) {
				a = generator.nextInt(100 - 60 + 1) + 60;
				b = 100 - a;
			}
			// grade 7 8 9
			else if (level < 9) {
				a = generator.nextInt(100 - 40 + 1) + 40;
				b = 100 - a;
			}
			// grade 10 11 12
			else {
				a = generator.nextInt(100 - 20 + 1) + 20;
				b = 100 - a;
			}

			switch (help1) {
			case "AB":
				if (corectAns.equals("A")) {
					prbA.setValue(a);
					prbB.setValue(b);
				} else {
					prbB.setValue(a);
					prbA.setValue(b);
				}
				prbC.setValue(0);
				prbD.setValue(0);
				break;
			case "AC":
				if (corectAns.equals("A")) {
					prbA.setValue(a);
					prbC.setValue(b);
				} else {
					prbC.setValue(a);
					prbA.setValue(b);
				}
				prbB.setValue(0);
				prbD.setValue(0);
				break;
			case "AD":
				if (corectAns.equals("A")) {
					prbA.setValue(a);
					prbD.setValue(b);
				} else {
					prbD.setValue(a);
					prbA.setValue(b);
				}
				prbB.setValue(0);
				prbC.setValue(0);
				break;
			case "BC":
				if (corectAns.equals("B")) {
					prbB.setValue(a);
					prbC.setValue(b);
				} else {
					prbC.setValue(a);
					prbB.setValue(b);
				}
				prbA.setValue(0);
				prbD.setValue(0);
				break;
			case "BD":
				if (corectAns.equals("B")) {
					prbB.setValue(a);
					prbD.setValue(b);
				} else {
					prbD.setValue(a);
					prbB.setValue(b);
				}
				prbA.setValue(0);
				prbC.setValue(0);
				break;
			case "CD":
				if (corectAns.equals("C")) {
					prbC.setValue(a);
					prbD.setValue(b);
				} else {
					prbD.setValue(a);
					prbC.setValue(b);
				}
				prbA.setValue(0);
				prbB.setValue(0);
				break;
			}
		}
	}
}

@SuppressWarnings("serial")
class LevelDialog extends JDialog {
	private int u = MyApp.ScreenUnit;
	private JPanel panel;
	private JButton btnLv[] = new JButton[12];
	private JButton btnOK;
	private int level;
	private AePlayWave player;

	LevelDialog(int lv) {
		super(MyApp.frmPlaying, ModalityType.APPLICATION_MODAL);
		initComponents();
		btnLv[lv].setBackground(Color.green);
		player = new AePlayWave("res/raw/lv" + (lv+1) + ".wav");
		this.level = lv;
	}

	private void initComponents() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/bkg_level.jpg");
			
			@Override
			public void paintComponent(Graphics g){
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(new Dimension(12*u, MyApp.ScreenSize.height));
		panel.setLayout(null);
		//end panel
		
		btnLv[0] = new JButton("Câu hỏi lớp 1");
		btnLv[0].setBounds(3*u, (int)(16.5*u), (int)(5.5*u), (int)(0.7*u));
		btnLv[0].setFocusPainted(false);
		panel.add(btnLv[0]);
		btnLv[1] = new JButton("Câu hỏi lớp 2");
		btnLv[1].setBounds(3*u, (int)(15.5*u), (int)(5.5*u), (int)(0.7*u));
		btnLv[1].setFocusPainted(false);
		panel.add(btnLv[1]);
		btnLv[2] = new JButton("Câu hỏi lớp 3");
		btnLv[2].setBounds(3*u, (int)(14.5*u), (int)(5.5*u), (int)(0.7*u));
		btnLv[2].setFocusPainted(false);
		panel.add(btnLv[2]);
		btnLv[3] = new JButton("Câu hỏi lớp 4");
		btnLv[3].setBounds(3*u, (int)(13.5*u), (int)(5.5*u), (int)(0.7*u));
		btnLv[3].setFocusPainted(false);
		panel.add(btnLv[3]);
		btnLv[4] = new JButton("Câu hỏi lớp 5");
		btnLv[4].setBounds(3*u, (int)(12.5*u), (int)(5.5*u), (int)(0.7*u));
		btnLv[4].setFocusPainted(false);
		panel.add(btnLv[4]);
		btnLv[5] = new JButton("Câu hỏi lớp 6");
		btnLv[5].setBounds(3*u, (int)(10.5*u), (int)(5.5*u), (int)(1*u));
		btnLv[5].setFocusPainted(false);
		panel.add(btnLv[5]);
		btnLv[6] = new JButton("Câu hỏi lớp 7");
		btnLv[6].setBounds(3*u, (int)(9.2*u), (int)(5.5*u), (int)(1*u));
		btnLv[6].setFocusPainted(false);
		panel.add(btnLv[6]);
		btnLv[7] = new JButton("Câu hỏi lớp 8");
		btnLv[7].setBounds(3*u, (int)(7.9*u), (int)(5.5*u), (int)(1*u));
		btnLv[7].setFocusPainted(false);
		panel.add(btnLv[7]);
		btnLv[8] = new JButton("Câu hỏi lớp 9");
		btnLv[8].setBounds(3*u, (int)(6.6*u), (int)(5.5*u), (int)(1*u));
		btnLv[8].setFocusPainted(false);
		panel.add(btnLv[8]);
		btnLv[9] = new JButton("Câu hỏi lớp 10");
		btnLv[9].setBounds(3*u, (int)(4.5*u), (int)(5.5*u), (int)(1.2*u));
		btnLv[9].setFocusPainted(false);
		panel.add(btnLv[9]);
		btnLv[10] = new JButton("Câu hỏi lớp 11");
		btnLv[10].setBounds(3*u, (int)(3*u), (int)(5.5*u), (int)(1.2*u));
		btnLv[10].setFocusPainted(false);
		panel.add(btnLv[10]);
		btnLv[11] = new JButton("Câu hỏi lớp 12");
		btnLv[11].setBounds(3*u, (int)(1.5*u), (int)(5.5*u), (int)(1.2*u));
		btnLv[11].setFocusPainted(false);
		panel.add(btnLv[11]);
		
		btnOK = new JButton();
		btnOK.setBounds((int)(8.5*u), 15*u, 3*u, 3*u);
		ImageIcon ico = new ImageIcon("res/drawable/icon/ok.png");
		Image img = ico.getImage().getScaledInstance(3*u, 3*u,
				Image.SCALE_SMOOTH);
		btnOK.setIcon(new ImageIcon(img));
		btnOK.setOpaque(false);
		btnOK.setContentAreaFilled(false);
		btnOK.setBorderPainted(false);
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				LevelDialog.this.disappear();
			}
		});
		panel.add(btnOK);
		
		this.setLocation(0, 0);
		this.setSize(panel.getSize());
		this.add(panel);
		this.setResizable(false);
		this.setUndecorated(true);
		pack();
	}
	
	public void appear() {
		final Timer timer = new Timer(5, null);
        timer.setRepeats(true);
        timer.addActionListener(new ActionListener() {
            private int x = -12*u;
            @Override public void actionPerformed(ActionEvent e) {
            	x += 5;
                LevelDialog.this.setLocation(x, 0);
                if (x >= 0) {
                	timer.stop();
                	player.start();
                	if(level != 0) {
            	        new Thread(new Runnable() {
            				@Override
            				public void run() {
            					while(player.isAlive());
            			        LevelDialog.this.disappear();
            				}
            			}).start();
                    }
                }
            }
        });
        timer.start();
        this.setVisible(true);
	}
	public void disappear(){
		int x = this.getLocation().x;
		do {
			x -= 5;
			this.setLocation(x, 0);
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while (x > -12*u);
		this.setVisible(false);
		this.dispose();
	}
}