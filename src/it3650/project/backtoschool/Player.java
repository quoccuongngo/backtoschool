package it3650.project.backtoschool;

import java.awt.Image;

public class Player {
	private String name;
	private String sex;
	private Image avatar;
	
	public Player(){
		
	}
	
	public Player(String name, String sex, Image avatar){
		this.name = name;
		this.sex = sex;
		this.avatar = avatar;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Image getAvatar() {
		return avatar;
	}

	public void setAvatar(Image avatar) {
		this.avatar = avatar;
	}
}
