package it3650.project.backtoschool;

public class Question {
	private String ID;
	private String ques;
	private String ansA;
	private String ansB;
	private String ansC;
	private String ansD;
	private String correctAns;
	private String help1;
	private String help2;
	
	public Question(String ID, String ques, 
			String ansA, String ansB, String ansC, String ansD,
			String correctAns, String help1, String help2) {
		this.ID = ID;
		this.ques = ques;
		this.ansA = ansA;
		this.ansB = ansB;
		this.ansC = ansC;
		this.ansD = ansD;
		this.correctAns = correctAns;
		this.help1 = help1;
		this.help2 = help2;
	}

	public String getID() {
		return ID;
	}

	public String getQues() {
		return ques;
	}

	public String getAnsA() {
		return ansA;
	}

	public String getAnsB() {
		return ansB;
	}

	public String getAnsC() {
		return ansC;
	}

	public String getAnsD() {
		return ansD;
	}

	public String getCorrectAns() {
		return correctAns;
	}

	public String getHelp1() {
		return help1;
	}

	public String getHelp2() {
		return help2;
	}
}
