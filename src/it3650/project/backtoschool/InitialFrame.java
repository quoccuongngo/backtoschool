package it3650.project.backtoschool;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class InitialFrame extends JFrame {
	private int u = MyApp.ScreenUnit;

	// Player info
	private String player_name;
	private String player_sex;
	private Image player_avt;

	// Display components
	private JPanel panel;
	private JTextField txtName;
	private ButtonGroup grpSex;
	private JRadioButton rdbMale;
	private JRadioButton rdbFemale;
	private JButton btnAvatar;
	private JButton btnStart;
	private JButton btnExit;
	private int avatar;
	
	class Avatar{
		public static final int MALE = 0;
		public static final int FEMALE = 1;
		public static final int CUSTOMIZED = 2;
	}
	public InitialFrame() {
		initComponents();
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Panel
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/init.jpg");

			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(MyApp.ScreenSize);
		panel.setLayout(null);
		//end panel

		// txtName
		txtName = new JTextField();
		// set hint
		final String hint_name = "Họ và Tên";
		txtName.setText(hint_name);
		txtName.setFont(new Font("Arial", 0, 30));
		txtName.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (txtName.getText().equals("")) {
					txtName.setText(hint_name);
				}
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				if (txtName.getText().equals(hint_name)) {
					txtName.setText("");
				}
			}
		});
		panel.add(txtName);
		txtName.setBounds(u*17/2, 11*u/2, 7*u, 2*u);
		// end txtName
		
		// Sex
		grpSex = new ButtonGroup();
		rdbMale = new JRadioButton();
		rdbFemale = new JRadioButton();
		rdbMale.setSelected(true);
		grpSex.add(rdbMale);
		grpSex.add(rdbFemale);
		panel.add(rdbMale);
		rdbMale.setBackground(Color.green);
		rdbMale.setBounds(9 * u, 12 * u, (int)(1.3*u), (int)(1.3*u));
		panel.add(rdbFemale);
		rdbFemale.setBounds(12 * u, 12 * u, (int)(1.3*u),(int)(1.3*u));
		rdbFemale.setBackground(Color.green);
		
		rdbMale.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (avatar == Avatar.FEMALE) {
					avatar = Avatar.MALE;
					ImageIcon icoAvt = new ImageIcon("res/drawable/icon/male.jpg");
					Image imgAvt = icoAvt.getImage().getScaledInstance(5 * u, 5 * u,
							Image.SCALE_SMOOTH);
					btnAvatar.setIcon(new ImageIcon(imgAvt));
				}
			}
		});
		rdbFemale.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (avatar == Avatar.MALE) {
					avatar = Avatar.FEMALE;
					ImageIcon icoAvt = new ImageIcon("res/drawable/icon/female.jpg");
					Image imgAvt = icoAvt.getImage().getScaledInstance(5 * u, 5 * u,
							Image.SCALE_SMOOTH);
					btnAvatar.setIcon(new ImageIcon(imgAvt));
				}
			}
		});
		// end sex

		// Avatar
		btnAvatar = new JButton();
		btnAvatar.setBounds(20 * u, (int)(10.8*u), 5 * u, 5 * u);
		ImageIcon icoAvt = new ImageIcon("res/drawable/icon/male.jpg");
		Image imgAvt = icoAvt.getImage().getScaledInstance(5 * u, 5 * u,
				Image.SCALE_SMOOTH);
		btnAvatar.setIcon(new ImageIcon(imgAvt));
		avatar = Avatar.MALE;
		btnAvatar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				btnAvatarActionPerformed(evt);
			}
		});
		btnAvatar.setToolTipText("Chọn ảnh đại diện");
		panel.add(btnAvatar);
		// end avatar

		// OK
		btnStart = new JButton();
		btnStart.setBounds(14 * u, 15 * u, 4 * u, 2 * u);
		ImageIcon icoOk = new ImageIcon("res/drawable/icon/ok.jpg");
		Image imgOk = icoOk.getImage().getScaledInstance(4 * u, 2 * u,
				Image.SCALE_SMOOTH);
		btnStart.setIcon(new ImageIcon(imgOk));
		btnStart.setOpaque(false);
		btnStart.setContentAreaFilled(false);
		btnStart.setBorderPainted(false);
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				btnOkActionPerformed(evt);
			}
		});
		panel.add(btnStart);
		// end ok

		//btnExit
		btnExit = new JButton();
		btnExit.setBounds(31*u, 0, u, u);
		ImageIcon icoExit = new ImageIcon("res/drawable/icon/exit.png");
		Image imgExit = icoExit.getImage().getScaledInstance(u, u, Image.SCALE_SMOOTH);
		btnExit.setIcon(new ImageIcon(imgExit));
		btnExit.setOpaque(false);
		btnExit.setContentAreaFilled(false);
		btnExit.setBorderPainted(false);
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		panel.add(btnExit);
		//end btnExit
		
		// Frame
		this.setIconImage(MyApp.icon);
		this.setUndecorated(true);
		this.setSize(MyApp.ScreenSize);
		this.add(panel);

		pack();
	}

	protected void btnOkActionPerformed(ActionEvent evt) {
		player_name = txtName.getText();
		if (grpSex.getSelection() == rdbFemale) {
			player_sex = "Nữ";
		} else {
			player_sex = "Nam";
		}
		player_avt = ((ImageIcon) btnAvatar.getIcon()).getImage();

		MyApp.player.setName(player_name);
		MyApp.player.setAvatar(player_avt);
		MyApp.player.setSex(player_sex);
		
		MyApp.frmNavigation = new NavigationFrame();
		MyApp.frmNavigation.setVisible(true);
		this.setVisible(false);
		this.dispose();
	}

	protected void btnAvatarActionPerformed(ActionEvent evt) {
		FileDialog fd = new FileDialog(this, "Choose a picture", FileDialog.LOAD);
		fd.setVisible(true);
		String filename = fd.getFile();
		if (filename == null) {
			return;
		}
		File chosenFile = new File(fd.getDirectory() + filename);
		try {
			Image avt = ImageIO.read(chosenFile)
					.getScaledInstance(btnAvatar.getWidth(), btnAvatar.getHeight(), Image.SCALE_SMOOTH);
			btnAvatar.setIcon(new ImageIcon(avt));
			avatar = Avatar.CUSTOMIZED;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Không mở được ảnh", "LỖI", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}