package it3650.project.backtoschool;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyApp {

	/**
	 * @param args
	 */
	public static int ScreenUnit;
	public static Dimension ScreenSize;
	public static ArrayList<ArrayList<Question>> QuestionCollection;
	public static Image icon;
	public static AePlayWave sound;
	
	public static int level;
	public static int difficulty = Difficulty.NORMAL;
	public static Player player;
	
	private static WelcomeFrame frmStart;
	public static InitialFrame frmInit;
	public static NavigationFrame frmNavigation;
	public static PlayingFrame frmPlaying;
	
	public class Difficulty{
		public static final int EASY = 1;
		public static final int NORMAL = 2;
		public static final int HARD = 3;
	}
	public static void main(String[] args) {
		initParams();
		icon = new ImageIcon("res/drawable/icon/icon.png").getImage();
		player = new Player();
		sound = new AePlayWave("res/raw/tinh_tho.wav");
		
		startGame();
		
	}

	private static void initParams() {
		icon = Toolkit.getDefaultToolkit().getImage("res/drawable/icon.png");
		ScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int unit_hight = (int) (((float) ScreenSize.height) / 18);
		int unit_width = (int) (((float) ScreenSize.width) / 32);
		ScreenUnit = Math.min(unit_hight, unit_width);
	}

	static void loadQuestion() throws SQLException {
		Connection conn = DriverManager.getConnection("jdbc:ucanaccess://res/bts.DAT");
		Statement s = conn.createStatement();
		ResultSet rs;
		
		QuestionCollection = new ArrayList<ArrayList<Question>>();
		for (int i = 0; i < 12; i++) {
			QuestionCollection.add(new ArrayList<Question>());
			
			int j = i+1;
			String c;
			if (j < 10) {
				c = "0" + j;
			} else {
				c = j + "";
			}
			
			rs = s.executeQuery("SELECT * FROM Question WHERE ID LIKE '" + c + "%'" );
			while (rs.next()) {
			    Question ques = new Question(
			    		//ID, ques, ansA, ansB, ansC, ansD,
						// correctAns, help1, help2
			    		rs.getString(1), rs.getString(2),
			    		rs.getString(3), rs.getString(4),
			    		rs.getString(5), rs.getString(6),
			    		rs.getString(7), rs.getString(8), rs.getString(9));
			    QuestionCollection.get(i).add(ques);
			}
		}
		conn.close();
	}
	
	@SuppressWarnings("deprecation")
	public static void startGame() {
		frmStart = new WelcomeFrame();
		if(!frmStart.show(3000)) {
			return;
		}
		sound.start();
		sound.suspend();
		restartGame();
	}
	
	@SuppressWarnings("deprecation")
	public static void restartGame() {
		level = 0;
		frmInit = new InitialFrame();
		frmInit.setVisible(true);
		
		sound.resume();
	}
}
@SuppressWarnings("serial")
class WelcomeFrame extends JFrame {
	JPanel panel;
	
	public WelcomeFrame() {
		initComponents();
	}
	
	private void initComponents() {
		int u = MyApp.ScreenUnit;
		panel = new JPanel() {
			ImageIcon icon = new ImageIcon("res/drawable/background/welcome.jpg");

			public void paintComponent(Graphics g) {
				Dimension d = getSize();
				g.drawImage(icon.getImage(), 0, 0, d.width, d.height, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		panel.setPreferredSize(new Dimension(16*u, 9*u));
		panel.setLayout(null);
		
		this.setIconImage(MyApp.icon);
		this.setUndecorated(true);
		this.setSize(MyApp.ScreenSize);
		this.add(panel);

		pack();
		this.setLocationRelativeTo(null);
	}
	
	public boolean show(long ms) {
		this.setVisible(true);
		try {
			MyApp.loadQuestion();
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Không tải được câu hỏi, vui lòng thử lại", "LỖI", JOptionPane.ERROR_MESSAGE);
			return false;
		} finally{
			this.setVisible(false);
			this.dispose();
		}
		return true;
	}
}